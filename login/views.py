from django.shortcuts import render, redirect
from .models import Account
from .forms import AccountForm
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
def registerPage(request):
    form=UserCreationForm()
    context = {'form' : form }
    return render(request,'login/register.html')

def index(request):
    account = Account.objects.all()
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login:index')
    else:
        form = AccountForm()        
    return render(request,'login/login.html',{
        'form' : form,
        'account':account,
    })