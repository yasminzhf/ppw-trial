from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *
from .urls import *
from .models import Account

# Create your tests here.
class LoginTest(TestCase):
    def test_login_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_login_is_using_login_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'login/login.html')

    def test_login_is_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_person(self):
        new_person = Account.objects.create(name ='kelompok ppw')

        counting_all_available_account = Account.objects.all().count()
        self.assertEqual(counting_all_available_account,1)

    def test_check_model_return_person(self):
        new_person = Account.objects.create(name ='kelompok ppw')
        result = Account.objects.get(id=1)
        self.assertEqual(str(result),'kelompok ppw')

    def test_add_account_post_url(self):
        response = Client().post('/', data={'name': 'kelompok ppw',})
        amount = Account.objects.filter(name="kelompok ppw").count()
        self.assertEqual(amount, 1)
